package net.nilosplace.sorting;

import java.util.List;

public class SelectionSortAlgorithm extends Algorithm {

	public SelectionSortAlgorithm(int[] data) {
		super(data);
	}

	@Override
	public List<Swap> sort() {
		
		for(int i = 0; i < localData.length - 1; i++) {
			int index = i;
			for(int j = i + 1; j < localData.length; j++) {
				if(localData[j] < localData[index]) {
					index = j;
				}
			}
			swap(i, index);
		}
		
		return swaps;
	}

}
