package net.nilosplace.sorting;

import java.util.ArrayList;
import java.util.List;

public abstract class Algorithm {

	protected int[] localData;
	protected List<Swap> swaps = new ArrayList<Swap>();
	
	public Algorithm(int[] data) {
		localData = new int[data.length];
		for(int i = 0; i < data.length; i++) {
			localData[i] = data[i];
		}
	}

	protected void swap(int left, int right) {
		if(left == right) return;
		int temp = localData[left];
		localData[left] = localData[right];
		localData[right] = temp;
		swaps.add(new Swap(left, right));
	}
	
	public abstract List<Swap> sort();

}
