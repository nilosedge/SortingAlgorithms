package net.nilosplace.sorting;

import java.util.List;

public class BubbleSortAlgorithm extends Algorithm {

	public BubbleSortAlgorithm(int[] data) {
		super(data);
	}
	
	@Override
	public List<Swap> sort() {

		boolean swapFlag = true;

		while(swapFlag) {
			swapFlag = false;
			for(int i = 0; i < localData.length - 1; i++) {
				if(localData[i] > localData[i + 1]) {
					swap(i, i + 1);
					swapFlag = true;
				}
			}
		}
		
		return swaps;
	}

}
