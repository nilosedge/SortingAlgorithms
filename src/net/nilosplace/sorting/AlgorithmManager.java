package net.nilosplace.sorting;

import java.util.ArrayList;
import java.util.List;

public class AlgorithmManager {

	private List<Algorithm> algos = new ArrayList<Algorithm>();
	
	public void add(Algorithm sortAlgorithm) {
		algos.add(sortAlgorithm);
	}

	public void draw() {
		
		for(Algorithm a: algos) {
			List<Swap> swaps = a.sort();
			System.out.println(swaps);
			System.out.println(swaps.size());
		}
	
	}

}
