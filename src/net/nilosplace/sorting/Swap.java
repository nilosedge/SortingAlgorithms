package net.nilosplace.sorting;

public class Swap {

	private int left;
	private int right;
	
	public Swap(int left, int right) {
		this.left = left;
		this.right = right;
	}

	public int getLeft() {
		return left;
	}
	public void setLeft(int left) {
		this.left = left;
	}
	public int getRight() {
		return right;
	}
	public void setRight(int right) {
		this.right = right;
	}
	
	public String toString() {
		return left + "<->" + right;
	}
}
