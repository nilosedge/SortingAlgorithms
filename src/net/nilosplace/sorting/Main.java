package net.nilosplace.sorting;


public class Main {

	public static void main(String[] args) {
		
		
		
		int[] randomData = new int[100];
		for(int i = 0; i < randomData.length; i++) {
			randomData[i] = (int)(Math.random() * randomData.length);
		}
		
		int[] sortedData = new int[100];
		for(int i = 0; i < sortedData.length; i++) {
			sortedData[i] = i + 1;
		}
		
		int[] sortedReverseData = new int[100];
		for(int i = 0; i < sortedReverseData.length; i++) {
			sortedReverseData[i] = 100 - i;
		}
		
		int[] randomNonUniqueData = new int[100];
		for(int i = 0; i < randomNonUniqueData.length; i++) {
			randomNonUniqueData[i] = (int)(Math.random() * 4) * 25;
		}
		
		AlgorithmManager am = new AlgorithmManager();
		
		am.add(new BubbleSortAlgorithm(randomData));
		am.add(new BubbleSortAlgorithm(sortedData));
		am.add(new BubbleSortAlgorithm(sortedReverseData));
		am.add(new BubbleSortAlgorithm(randomNonUniqueData));
		
		am.add(new InsertionSortAlgorithm(randomData));
		am.add(new InsertionSortAlgorithm(sortedData));
		am.add(new InsertionSortAlgorithm(sortedReverseData));
		am.add(new InsertionSortAlgorithm(randomNonUniqueData));
		
		am.add(new SelectionSortAlgorithm(randomData));
		am.add(new SelectionSortAlgorithm(sortedData));
		am.add(new SelectionSortAlgorithm(sortedReverseData));
		am.add(new SelectionSortAlgorithm(randomNonUniqueData));
		
		am.draw();
		
	}

}
