package net.nilosplace.sorting;

import java.util.List;

public class InsertionSortAlgorithm extends Algorithm {

	public InsertionSortAlgorithm(int[] data) {
		super(data);
	}

	@Override
	public List<Swap> sort() {
		
		for(int i = 1; i < localData.length; i++) {
			int j = 0;
			for(j = i - 1; j >= 0 && localData[i] < localData[j]; j--) {
				swap(j, j + 1);
			}
			swap(i, j + 1);
		}
		
		return swaps;
	}

}
